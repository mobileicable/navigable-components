'use strict'

module.exports = {
  plugins: [
    'plugins/markdown',
    'node_modules/jsdoc-vuejs'
  ],
  source: {
    include: ['./src'],
    includePattern: '.+\\.(vue|js)$'
  },
  opts: {
    template: 'node_modules/minami',
    destination: './docs/',
    readme: './README.md',
    recurse: true
  }
}
