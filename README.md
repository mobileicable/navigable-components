# Navigable Components

Vue UI components supporting both keyboard and mouse navigation for Smart TVs and browsers.

## Project setup
```sh
npm install
```

## Usage

### Build as library
```sh
npm run build
```
Source will be build as CommonJS and UMD bundles. Built files will be created in `./dist`

### Install by files
The library can be used by copying-and-pasting the built files to the target project by the following steps.
1. Create a folder named `navigable-components` under the `node_modules` folder of the project
2. Copy `./package.json` to the created folder, and all files in `./dist` to a subfolder named `dist`
3. Done

### Install by Git
The library can be used by getting the built files from its Git repository.
```sh
npm install git+https://<my-repo-domain>/navigable-components.git --save
```

### Import
```javascript
// import CSS of the components
import '../node_modules/navigable-components/dist/navigable-components.css'
// import the required components
import { /* Classes, mixins or components to be imported */ } from 'navigable-components'
```

## Development

### Run demo / Compiles and hot-reloads for development
```sh
npm run serve
```

### Generate documentation for reference
```sh
npm run doc
```
Documentation will be generated in `./docs`, open `index.html` to check the generated content.

### Start using
1. All `Navigable` components should be (sub-)children of a single parent `NavigableViewport` component (e.g. `View` or `ScrollView`)
```html
<template>
    <div id="app">
        <scroll-view ref="container" class="container">
            <!-- ... -->
        </scroll-view>
    </div>
</template>
```
2. The CSS `position` of the top-most parent component *must not* be `static`
```css
.container { position: relative; }
```
3. On DOM ready, or on the parent component mounted, run `init()` for once
```javascript
mounted () {
    NavigationManager.instance.init()
    this.$refs.container.focus()
    // ...
}
```

### Deployment
When the source is ready for deployment, remember to:
1. Update version code
2. Build the project as library
3. Push with the built files in `./dist` to the master branch