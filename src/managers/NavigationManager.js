import EventEmitter from 'events'
/**
 * Provides an overview on the current focusing components on all layers and handles key mapping and events.
 * Individual navigation groupings can be divided by layers, usually for modals, popups.
 * **Do not call constructor explicitly, use singleton instance instead.**
 * @class NavigationManager
 * @hideconstructor
 */
export default class NavigationManager extends EventEmitter {
  /**
   * @typedef {Object} Keymap
   * @property {Number} [left=37] Key code of Left key
   * @property {Number} [top=38] Key code of Up key
   * @property {Number} [right=39] Key code of Right key
   * @property {Number} [bottom=40] Key code of Down key
   * @property {Number} [enter=13] Key code of Enter key
   */

  /**
   * @var {String} activeLayer The layer that the focused Navigable located on, and the next navigation will be act on
   * @memberof NavigationManager
   * @readonly
   * @instance
   */
  get activeLayer () {
    return this._activeLayer
  }
  set activeLayer (newValue) {
    let isChanged = this._activeLayer !== newValue
    this._activeLayer = newValue
    /**
     * Fired when the active layer changed
     * @event active-layer-change
     * @memberof NavigationManager
     * @instance
     */
    if (isChanged) this.emit('active-layer-changed', this.activeLayer)
  }

  /**
   * @var {Boolean} [isCacheEnabled=false] Set true to apply cache for Navigable rects and dimensions
   * @memberof NavigationManager
   * @instance
   */
  get isCacheEnabled () {
    return this._isCacheEnabled
  }
  set isCacheEnabled (newValue) {
    this._isCacheEnabled = !!newValue
    if (!this._isCacheEnabled) this.clearCache()
  }

  // Not to be called explicitly, use singleton instance instead
  constructor () {
    super()

    this._cachedRects = {}
    this._cachedDimensions = {}

    this._isCacheEnabled = false
    this._isSuspending = false
    this._suspendTimer = null
    this._currentMounted = null
    this._activeLayer = 'default'
    this._currentFocused = { default: null }
    this._defaultKeymap = {
      left: 37,
      top: 38,
      right: 39,
      bottom: 40,
      enter: 13
    }
    this._keymap = this._defaultKeymap
    this._onKeyDown = function (ev) {
      let key = ev.keycode || ev.which
      let keyName = Object.values(this._keymap).indexOf(key) > -1 ? Object.keys(this._keymap).find(k => this._keymap[k] === key) : null
      let focused = this.getFocused(this.activeLayer)
      if (focused && keyName) {
        let isHandled = false
        isHandled = isHandled || this._isSuspending
        isHandled = isHandled || focused.handleNavigationKeyDown(keyName)
        isHandled = isHandled || (keyName === 'enter' && !!focused.$emit('click', focused))
        isHandled = isHandled || focused.navTo(keyName)
        if (isHandled) {
          ev.preventDefault()
          return false
        }
      }
    }.bind(this)
  }
  /**
   * Initialize the manager, on the document or a specific DOM element, with a keymap defining the direction keys and enter key
   * @param {Keymap} [keymap] A keymap defining the key codes of direction keys and enter key
   * @param {HTMLElement} [element=document] The element that key events attached to
   * @memberof NavigationManager
   * @instance
   */
  init (keymap, element = document) {
    this._keymap = Object.assign(Object.assign({}, this._defaultKeymap), keymap || {})
    if (this._currentMounted) this._currentMounted.removeEventListener('keydown', this._onKeyDown)
    element.addEventListener('keydown', this._onKeyDown)
    this._currentMounted = element
  }
  /**
   * Get or set the current focused Navigable on a specific layer
   * @param {String|Navigable} [navigableOrLayer="default"] Name of the desired layer or empty for default to get, or the Navigable to set
   * @param {String} [device="keyboard"] The device used to trigger focus ("keyboard"/"mouse")
   * @return {?Navigable} The focusing Navigable when it is getting
   * @memberof NavigationManager
   * @method focus
   * @instance
   * @deprecated Obsoleted since version 0.1.6, please use "getFocused" instead.
   */
  /**
   * Set the current focused Navigable
   * @param {Navigable} navigable The Navigable to set focused
   * @param {String} [device="keyboard"] The device used to trigger focus ("keyboard"/"mouse")
   * @memberof NavigationManager
   * @instance
   */
  focus (navigableOrLayer = 'default', device = 'keyboard') {
    if (typeof navigableOrLayer === 'string') { // get
      console.warn('[NavigationManager] Method \'focus(string)\' is obsoleted and will be removed soon. Please use \'getFocused\' instead.') // eslint-disable-line
      return this._currentFocused[navigableOrLayer]
    } else { // set
      if (!navigableOrLayer.isNavigable) throw new TypeError()
      if (this.getFocused(navigableOrLayer.navLayer) === navigableOrLayer) {
        this.activeLayer = navigableOrLayer.navLayer
      } else {
        navigableOrLayer.focusedBy = device || 'keyboard'
        if (this.getFocused(navigableOrLayer.navLayer) !== navigableOrLayer) this.blur(navigableOrLayer.navLayer)
        this._currentFocused[navigableOrLayer.navLayer] = navigableOrLayer
        this.activeLayer = navigableOrLayer.navLayer
      }
    }
  }
  /**
   * Get the current focused Navigable on a specific layer
   * @param {String} [layer="default"] Name of the desired layer to get
   * @return {?Navigable} The focusing Navigable
   * @memberof NavigationManager
   * @instance
   */
  getFocused (layer = 'default') {
    return this._currentFocused[layer] || null
  }
  /**
   * Unfocus the current focused Navigable on a specific layer
   * @param {String} [layer="default"] Name of the blurring layer
   * @memberof NavigationManager
   * @instance
   */
  blur (layer = 'default') {
    if (this._currentFocused[layer]) this._currentFocused[layer].focusedBy = null
    this._currentFocused[layer] = null
  }
  /**
   * Suspend keyboard navigation, for a specific duration optionally
   * @param {Number} [timeout] Suspend duration in milliseconds
   */
  suspend (timeout) {
    this._isSuspending = true
    if (typeof timeout !== 'undefined') {
      this._suspendTimer = setTimeout(() => {
        this._isSuspending = false
      }, timeout)
    }
  }

  /**
   * Resume keyboard navigation from suspending immediately
   */
  resume () {
    clearTimeout(this._suspendTimer)
    this._isSuspending = false
  }

  clearCache () {
    this._cachedRects = {}
    this._cachedDimensions = {}
  }

  cachedRect (uid, rect) {
    if (!this._isCacheEnabled) return
    if (!rect) return this._cachedRects[uid]
    else this._cachedRects[uid] = rect
  }

  cachedDimensions (uid, dimensions) {
    if (!this._isCacheEnabled) return
    if (!dimensions) return this._cachedDimensions[uid]
    else this._cachedDimensions[uid] = dimensions
  }
}

/**
 * Singleton instance of NavigationManager
 * @member {NavigationManager} instance
 * @memberof NavigationManager
 * @static
 */
let navigationManager = null
Object.defineProperty(NavigationManager, 'instance', {
  get () {
    if (!navigationManager) navigationManager = new NavigationManager()
    return navigationManager
  }
})
