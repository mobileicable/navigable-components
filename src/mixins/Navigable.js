import NavigationManager from '../managers/NavigationManager'

/**
 * @typedef {Object} Rect
 * @property {Number} top The top offset in pixel
 * @property {Number} bottom The top offset in pixel
 * @property {Number} left The left offset in pixel
 * @property {Number} right The right offset in pixel
 * @property {Number} width The width in pixel
 * @property {Number} height The height in pixel
 */

/**
 * The mixin for implementing a keyboard navigation enabled component.
 * The implemented component should not have any Navigable child.
 * For implementing containers, use NavigableViewport.
 * @name Navigable
 * @mixin
 *
 * @property {Boolean} [disabled=false] - Set to true to disable the navigation of the instance
 * @property {String} [layer="default"] - Navigation layer of the instance
 * @property {String} [translateMethod="translate"] - The mechanism used for translation ("translate" / "translate3d" / "position" / "margin"), effect depends on component implementation
 */
export default {
  /**
   * Fired when the Navigable instance is clicked
   * @event click
   * @memberof Navigable
   */
  /**
   * Fired when the Navigable instance is focused
   * @event focus
   * @memberof Navigable
   */
  /**
   * Fired when the Navigable instance is blurred
   * @event blur
   * @memberof Navigable
   */
  data () {
    return {
      /**
       * Device used to focus the instance ("keyboard"/"mouse"), null if it is not currently focused
       * @member {String}
       * @memberof Navigable
       * @instance
       */
      focusedBy: null,
      /**
       * True if the instance can be navigated to
       * @member {Boolean}
       * @memberof Navigable
       * @instance
       */
      isEnabled: true,
      /**
       * Refer record of the last navigation
       * @member {Object}
       * @memberof Navigable
       * @instance
       * @property {?String} referer.direction - The referer direction
       * @property {?Navigable} referer.navigable - The referer Navigable
       */
      referer: {
        direction: null,
        navigable: null
      },
      /**
       * Custom destination Navigables
       * @member {Object}
       * @memberof Navigable
       * @instance
       * @property {?Navigable} top - The custom destination Navigable when navigate from the instance to the top
       * @property {?Navigable} bottom - The custom destination Navigable when navigate from the instance to the bottom
       * @property {?Navigable} left - The custom destination Navigable when navigate from the instance to the left
       * @property {?Navigable} right - The custom destination Navigable when navigate from the instance to the right
       */
      destinations: {
        left: null,
        right: null,
        top: null,
        bottom: null
      }
    }
  },
  computed: {
    /**
     * True if the instance is currently focused
     * @type {Boolean}
     * @memberof Navigable
     * @instance
     */
    isFocused () {
      return this.focusedBy !== null
    },
    /**
     * A flag indicating the instance is a Navigable
     * @member {Boolean}
     * @memberof Navigable
     * @readonly
     * @instance
     */
    isNavigable () { return true },
    /**
     * The belonging NavigableViewport of the instance
     * @member {?NavigableViewport}
     * @memberof Navigable
     * @readonly
     * @instance
     */
    viewport: {
      cache: false,
      get () {
        let current = this
        do {
          let parent = current.$parent
          if (parent && parent.isNavigable) return parent
          current = parent
        } while (current)
        return null
      }
    },
    /**
     * The Navigable siblings of the instance
     * @member {Array.<Navigable>}
     * @memberof Navigable
     * @readonly
     * @instance
     */
    siblings: {
      cache: false,
      get () {
        let siblings = this.viewport ? this.viewport.children : []
        let me = siblings.findIndex(s => s._uid === this._uid)
        if (me > -1) siblings.splice(me, 1)
        return siblings
      }
    },
    /**
     * The width and height of the instance
     * @member {Rect}
     * @memberof Navigable
     * @readonly
     * @instance
     */
    dimensions: {
      cache: false,
      get () {
        let cache = NavigationManager.instance.cachedDimensions(this._uid)
        if (cache) return cache

        let offsets = { top: 0, left: 0, bottom: this.$el.offsetHeight, right: this.$el.offsetWidth, width: this.$el.offsetWidth, height: this.$el.offsetHeight }
        NavigationManager.instance.cachedDimensions(this._uid, offsets)
        return offsets
      }
    },
    /**
     * The offsets (relative to the viewport) and measurements of the instance
     * @member {Rect}
     * @memberof Navigable
     * @readonly
     * @instance
     */
    rect: {
      cache: false,
      get () {
        let cache = NavigationManager.instance.cachedRect(this._uid)
        if (cache) return cache

        let offsets = this.dimensions
        let defaultOffsets = offsets

        if (this.viewport) {
          let currentOrigin = this.$el
          do {
            offsets.top += currentOrigin.offsetTop
            offsets.left += currentOrigin.offsetLeft
            currentOrigin = currentOrigin.offsetParent
            // viewport must set CSS position
          } while (currentOrigin && currentOrigin !== this.viewport.$el)
          if (!currentOrigin) return defaultOffsets

          if (this.viewport.scrollBody) {
            // adjustment for different offset hierarchy when using 'position' and 'margin'
            offsets.top -= this.viewport.scrollBody.offsetTop
            offsets.left -= this.viewport.scrollBody.offsetLeft
          }
          offsets.top += this.viewport.translate.top
          offsets.left += this.viewport.translate.left
        }

        offsets.bottom = offsets.top + offsets.height
        offsets.right = offsets.left + offsets.width

        NavigationManager.instance.cachedRect(this._uid, offsets)
        return offsets
      }
    },
    /**
     * The actual destination Navigable when navigate from the instance to the left
     * @member {?Navigable}
     * @memberof Navigable
     * @instance
     */
    left: {
      cache: false,
      get () { return this.getDestination('left') },
      set (navigable) { this.setDestination('left', navigable) }
    },
    /**
     * The actual destination Navigable when navigate from the instance to the right
     * @member {?Navigable}
     * @memberof Navigable
     * @instance
     */
    right: {
      cache: false,
      get () { return this.getDestination('right') },
      set (navigable) { this.setDestination('right', navigable) }
    },
    /**
     * The actual destination Navigable when navigate from the instance to the top
     * @member {?Navigable}
     * @memberof Navigable
     * @instance
     */
    top: {
      cache: false,
      get () { return this.getDestination('top') },
      set (navigable) { this.setDestination('top', navigable) }
    },
    /**
     * The actual destination Navigable when navigate from the instance to the bottom
     * @member {?Navigable}
     * @memberof Navigable
     * @instance
     */
    bottom: {
      cache: false,
      get () { return this.getDestination('bottom') },
      set (navigable) { this.setDestination('bottom', navigable) }
    },
    /**
     * The belonging navigation layer
     * @member {String}
     * @memberof Navigable
     * @readonly
     * @instance
     */
    navLayer: {
      cache: false,
      get () {
        let current = this
        while (current && !current.layer) current = current.viewport
        return current ? current.layer : 'default'
      }
    }
  },
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    layer: {
      type: String,
      default: null
    },
    translateMethod: {
      type: String,
      default: 'translate',
      validator (v) {
        return ['translate', 'translate3d', 'position', 'margin'].indexOf(v) !== -1
      }
    }
  },
  watch: {
    isFocused () {
      if (this.isFocused) this.$emit('focus', this)
      else this.$emit('blur', this)
    },
    disabled: {
      immediate: true,
      handler () { this.isEnabled = !this.disabled }
    }
  },
  created () {
    this._getDestinationByRect = (siblings, rect, direction) => {
      let corner = {}
      let compare = {}

      if (direction === 'left') {
        corner = { close: 'right', far: 'left', acw: 'bottom', cw: 'top' }
        compare = {
          closerThan: (a, b) => { return a >= b },
          fartherThan: (a, b) => { return a <= b },
          isClockwiseOf: (a, b) => { return a < b },
          isAntiClockwiseOf: (a, b) => { return a > b },
          sorter: (a, b) => { return b - a }
        }
      }
      if (direction === 'right') {
        corner = { close: 'left', far: 'right', acw: 'top', cw: 'bottom' }
        compare = {
          closerThan: (a, b) => { return a <= b },
          fartherThan: (a, b) => { return a >= b },
          isClockwiseOf: (a, b) => { return a > b },
          isAntiClockwiseOf: (a, b) => { return a < b },
          sorter: (a, b) => { return a - b }
        }
      }
      if (direction === 'top') {
        corner = { close: 'bottom', far: 'top', acw: 'left', cw: 'right' }
        compare = {
          closerThan: (a, b) => { return a >= b },
          fartherThan: (a, b) => { return a <= b },
          isClockwiseOf: (a, b) => { return a > b },
          isAntiClockwiseOf: (a, b) => { return a < b },
          sorter: (a, b) => { return b - a }
        }
      }
      if (direction === 'bottom') {
        corner = { close: 'top', far: 'bottom', acw: 'right', cw: 'left' }
        compare = {
          closerThan: (a, b) => { return a <= b },
          fartherThan: (a, b) => { return a >= b },
          isClockwiseOf: (a, b) => { return a < b },
          isAntiClockwiseOf: (a, b) => { return a > b },
          sorter: (a, b) => { return a - b }
        }
      }

      // select all siblings within the area extending from one edge, overlapping more and 1 pixel
      // and then sort them from close to far
      let possibles = siblings.filter(s => {
        if (s.navLayer !== this.navLayer) return false
        let sRect = s.rect
        s.cachedRect = sRect
        let points = [
          rect[corner.acw], rect[corner.cw],
          sRect[corner.acw], sRect[corner.cw]
        ].sort((a, b) => a - b)
        let overlap = Math.abs(points[1] - points[2])
        return (compare.fartherThan(sRect[corner.close], rect[corner.far])) &&
          (compare.isAntiClockwiseOf(sRect[corner.acw], rect[corner.cw])) &&
          (compare.isClockwiseOf(sRect[corner.cw], rect[corner.acw])) &&
          overlap > 1
      }).sort((a, b) => {
        let aRect = a.cachedRect || a.rect
        let bRect = b.cachedRect || b.rect
        return compare.sorter(aRect[corner.close], bRect[corner.close])
      })
      // remove all siblings behind another one
      if (possibles.length) {
        if (possibles.length === 1) return possibles[0]
        let tempPossibles = [ possibles[0] ]
        let farEnds = possibles.map(s => (s.cachedRect || s.rect)[corner.far])
        for (let i = 1; i < possibles.length; i++) {
          let sRect = possibles[i].cachedRect || possibles[i].rect
          if (farEnds.findIndex(e => compare.fartherThan(sRect[corner.close], e)) > -1) break
          tempPossibles.push(possibles[i])
        }
        possibles = tempPossibles
      }
      // choose the closest and left/top most one
      if (possibles.length) {
        if (possibles.length === 1) return possibles[0]
        let getDistance = (p) => {
          let pRect = p.cachedRect || p.rect
          return Math.abs(pRect[corner.close] - rect[corner.far])
        }
        let closest = possibles[0]
        let closestDistance = getDistance(closest)
        for (let i = 1; i < possibles.length; i++) {
          if (getDistance(possibles[i]) === closestDistance) {
            let pRect = possibles[i].cachedRect || possibles[i].rect
            if (direction === 'left' || direction === 'right') {
              if (pRect.top < rect.top) closest = possibles[i]
            } else {
              if (pRect.left < rect.left) closest = possibles[i]
            }
          } else break
        }
        return closest
      }
      /*
      // choose the closest with bigger overlapping edge
      if (possibles.length) {
        if (possibles.length === 1) return possibles[0]
        let lastOverlap = 0
        for (let i = 0; i < possibles.length; i++) {
          let pRect = possibles[i].cachedRect || possibles[i].rect
          let points = [
            rect[corner.acw], rect[corner.cw],
            pRect[corner.acw], pRect[corner.cw]
          ].sort((a, b) => a - b)
          let overlap = Math.abs(points[1] - points[2])
          if (overlap < lastOverlap) return possibles[i - 1]
          lastOverlap = overlap
        }
        return possibles[possibles.length - 1]
      }
      */
      return null
    }
    this._onMouseOver = function (ev) {
      this.focus('mouse')
      ev.stopPropagation()
      return false
    }.bind(this)
    this._onClick = function (ev) {
      this.$emit('click', this)
      ev.stopPropagation()
      return false
    }.bind(this)
  },
  mounted () {
    this.$el.addEventListener('mouseover', this._onMouseOver)
    this.$el.addEventListener('click', this._onClick)
    if (this.viewport) this.viewport.onChildMounted(this)
  },
  destroyed () {
    this.$el.removeEventListener('mouseover', this._onMouseOver)
    this.$el.removeEventListener('click', this._onClick)
    if (this.viewport) this.viewport.onChildDestroyed(this)
  },
  methods: {
    /**
     * Set custom destination of the specific direction
     * @param {String} direction Direction of destination
     * @param {Navigable} navigable Destination Navigable
     * @param {Boolean} [isTwoWay] If true, set navigation bi-directionaly
     * @memberof Navigable
     * @instance
     * @method
     */
    setDestination (direction, navigable, isTwoWay) {
      if (!navigable.isNavigable) return
      if (typeof this.destinations[direction] !== 'undefined') {
        this.destinations[direction] = navigable || null
        if (isTwoWay) {
          let opposite = { left: 'right', right: 'left', top: 'bottom', bottom: 'top' }[direction]
          navigable.setDestination(opposite, this)
        }
      }
    },
    /**
     * Get actual destination of the specific direction
     * @param {String} direction Direction of destination
     * @param {Navigable} [origin] The Navigable navigating from
     * @returns {?Navigable} The actual destination of the specific direction
     * @memberof Navigable
     * @instance
     * @method
     */
    getDestination (direction, origin) {
      NavigationManager.instance.clearCache()
      if (typeof origin === 'undefined') origin = this

      // overridden
      if (this.destinations[direction]) {
        return this.destinations[direction]
      }

      // if it is going back
      let opposite = { left: 'right', right: 'left', top: 'bottom', bottom: 'top' }[direction]
      if (this.referer.direction === opposite) return this.referer.navigable

      // try get a sibling destination
      let sibling = this._getDestinationByRect(this.siblings, this.rect, direction)
      if (sibling) return sibling.getEntry(direction, origin)

      // ask parent if no sibling was found
      if (this.viewport) {
        let relative = this.viewport.getDestination(direction, origin)
        if (relative) return relative.getEntry(direction, origin)
      }

      return null
    },
    /**
     * Get the instance
     * @returns {Navigable} The instance
     * @memberof Navigable
     * @instance
     * @method
     */
    getEntry () {
      return this
    },
    /**
     * Get rect relative to a specific upstream viewport
     * @param {NavigationViewport} viewport An upstream viewport relative to
     * @returns {Rect}
     * @memberof Navigable
     * @instance
     * @method
     */
    getRectRelativeTo (viewport) {
      let rect = this.dimensions
      let currentOrigin = this
      while (currentOrigin && currentOrigin !== viewport) {
        let cRect = currentOrigin.rect
        rect.top += cRect.top
        rect.left += cRect.left
        currentOrigin = currentOrigin.viewport
      }
      if (!currentOrigin) return null
      rect.right = rect.left + rect.width
      rect.bottom = rect.top + rect.height
      return rect
    },
    /**
     * Navigate to a specific direction
     * @param {String} direction Navigation direction
     * @returns {Boolean} Returns false if no destination is found
     * @memberof Navigable
     * @instance
     * @method
     */
    navTo (direction) {
      let destination = this.getDestination(direction)
      if (destination) {
        destination.referer.direction = direction
        destination.referer.navigable = NavigationManager.instance.getFocused(this.navLayer)
        destination.focus()
        return true
      }
      return false
    },
    /**
     * Focus to the entry Navigable of the instance
     * @memberof Navigable
     * @instance
     * @method
     */
    focus (device = 'keyboard') {
      let entry = this.getEntry()
      if (entry) NavigationManager.instance.focus(entry, device || 'keyboard')
    },
    /**
     * Remove focus from the entry Navigable of the instance
     * @memberof Navigable
     * @instance
     * @method
     */
    blur () {
      let entry = this.getEntry()
      if (entry && NavigationManager.instance.getFocused(entry.navLayer) === entry) {
        NavigationManager.instance.blur(entry.navLayer)
      }
    },
    /**
     * Override this method to subscribe and interrupt key down event
     * from navigation manager
     * @param {String} keyName The name of the involving key
     * @return {Boolean} Return true to indicate an interrupt and no further action should be done
     * @memberof Navigable
     * @instance
     * @method
     */
    handleNavigationKeyDown (keyName) {
      return false
    }
  }
}
