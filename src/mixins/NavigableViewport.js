import Navigable from './Navigable'

/**
 * @typedef {String} ScrollMode
 * @property {String} horizontal
 * @property {String} vertical
 * @property {String} both
 * @property {String} none
 */

/**
 * The mixin for implementing a keyboard navigation enabled parent component.
 * The implemented component should redirect incoming focus to its child component (entry).
 * For implementing child component, use Navigable.
 * @name NavigableViewport
 * @augments Navigable
 * @mixin
 *
 * @property {ScrollMode} [scrollMode="none"] Possible scroll directions
 * @property {Number|Array} [scrollPadding=0] Focused item padding from the container in pixel, follow CSS specification for multiple values
 * @property {Boolean} [focusable=false] Allow focusing on the viewport itself when it is empty if set to true
 */
export default {
  /**
   * Fired when the a child of the instance is focused
   * @event child-focus
   * @memberof NavigableViewport
   */

  mixins: [Navigable],
  data () {
    return {
      focusedBy: null,
      /**
       * True if the instance has no child
       * @member {Boolean}
       * @memberof NavigableViewport
       * @instance
       */
      isEmpty: true,
      /**
       * The currently focused direct child Navigable or NavigableViewport
       * @member {?Navigable}
       * @memberof NavigableViewport
       * @instance
       */
      focusedChild: null,
      /**
       * Flags indicating if the instance should be translated along specific directions
       * @member {Object}
       * @memberof NavigableViewport
       * @instance
       * @property {Boolean} x True if the horizontal scroll is enabled for the instance
       * @property {Boolean} y True if the vertical scroll is enabled for the instance
       */
      canTranslate: {
        x: false,
        y: false
      },
      /**
       * Offsets in pixels that the instance should be translated to view the focused child
       * @member {Object}
       * @memberof NavigableViewport
       * @instance
       * @property {Number} top The top offset in pixels to be translated
       * @property {Number} left The left offset in pixels to be translated
       */
      translate: {
        top: 0,
        left: 0
      },
      /**
       * Translate paddings in pixels, parsed scrollPadding is stored here
       * @member {Object}
       * @memberof NavigableViewport
       * @instance
       * @property {Number} top The top padding for the focused item
       * @property {Number} bottom The bottom padding for the focused item
       * @property {Number} left The left padding for the focused item
       * @property {Number} right The right padding for the focused item
       */
      translatePadding: {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
      }
    }
  },
  props: {
    scrollMode: {
      type: String,
      default: 'none',
      validator (v) {
        return ['horizontal', 'vertical', 'both', 'none'].indexOf(v) !== -1
      }
    },
    scrollPadding: {
      type: [Number, Array],
      default: 0,
      validator (v) {
        return Array.isArray(v) ? v.length && v.reduce((p, c) => { return p && typeof c === 'number' }, true) : true
      }
    },
    focusable: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    /**
     * True if a child of the instance is currently focused
     * @member {Boolean}
     * @memberof NavigableViewport
     * @override
     * @instance
     */
    isFocused () {
      return this.focusedBy !== null
    },
    /**
     * True if the instance is a viewport
     * @member {Boolean}
     * @memberof Navigable
     * @readonly
     * @instance
     */
    isViewport () { return true },
    /**
     * The Navigable children of the instance
     * @member {Array.<Navigable>}
     * @memberof Navigable
     * @readonly
     * @instance
     */
    children: {
      cache: false,
      get () { return this.$children.filter(c => c.isNavigable && c.isEnabled && (!c.isViewport || !c.isEmpty || c.focusable)) }
    }
  },
  watch: {
    focusedChild (v) {
      this.focusedBy = v && v.focusedBy
      if (this.focusedBy) this.$emit('child-focus', this)
    },
    translatePadding: {
      deep: true,
      handler (v) {
        this.translate.top = v.top
        this.translate.left = v.left
      }
    },
    scrollMode: {
      immediate: true,
      handler (v) {
        this.canTranslate.x = v === 'horizontal' || v === 'both'
        this.canTranslate.y = v === 'vertical' || v === 'both'
      }
    },
    scrollPadding: {
      immediate: true,
      handler (v) {
        let _v = v
        if (typeof _v === 'number') _v = [v]
        if (Array.isArray(_v)) {
          // follow CSS order
          this.translatePadding.top = _v[0]
          this.translatePadding.right = _v[_v.length > 1 ? 1 : 0]
          this.translatePadding.bottom = _v[_v.length > 2 ? 2 : 0]
          this.translatePadding.left = _v[_v.length > 3 ? 3 : _v.length > 1 ? 1 : 0]
        }
      }
    }
  },
  mounted () {
    this.$el.removeEventListener('mouseover', this._onMouseOver)
    this.$el.removeEventListener('click', this._onClick)
  },
  methods: {
    /**
     * Get the entry child Navigable of the instance
     * @param {String} direction The incoming direction
     * @param {Navigable} origin The Navigable navigating from
     * @returns {?Navigable} The entry child Navigable of the instance
     * @memberof NavigableViewport
     * @instance
     * @method
     * @override
     */
    getEntry (direction, origin, commonViewport) {
      // TODO: provide way to override

      if (this.focusable && this.isEmpty) return this

      if (direction && origin) {
        if (typeof commonViewport === 'undefined') {
          commonViewport = this.viewport
          origin.cachedRect = origin.getRectRelativeTo(commonViewport)
        }

        if (this.children.length > 1) {
          // relate both rects to the common viewport
          this.cachedRect = this.getRectRelativeTo(commonViewport)

          // create virtual rect to look from
          let virtualRect = null
          if (direction === 'top' || direction === 'bottom') {
            virtualRect = {}
            virtualRect.left = Math.max(origin.cachedRect.left, this.cachedRect.left)
            virtualRect.right = Math.min(origin.cachedRect.right, this.cachedRect.right)
            virtualRect.width = virtualRect.right - virtualRect.left
            virtualRect.top = virtualRect.bottom = direction === 'top' ? this.cachedRect.bottom : this.cachedRect.top
            virtualRect.height = 0
            if (virtualRect.width <= 0) virtualRect = null // no overlap
          }
          if (direction === 'left' || direction === 'right') {
            virtualRect = {}
            virtualRect.top = Math.max(origin.cachedRect.top, this.cachedRect.top)
            virtualRect.bottom = Math.min(origin.cachedRect.bottom, this.cachedRect.bottom)
            virtualRect.height = virtualRect.bottom - virtualRect.top
            virtualRect.left = virtualRect.right = direction === 'left' ? this.cachedRect.right : this.cachedRect.left
            virtualRect.width = 0
            if (virtualRect.height <= 0) virtualRect = null // no overlap
          }
          if (virtualRect) {
            // relate virtual rect to this
            virtualRect.left -= this.cachedRect.left
            virtualRect.right -= this.cachedRect.left
            virtualRect.top -= this.cachedRect.top
            virtualRect.bottom -= this.cachedRect.top
            // look within children
            let child = this._getDestinationByRect(this.children, virtualRect, direction)
            if (child) return child.getEntry(direction, origin, commonViewport)
          }
        }
      }

      if (this.children.length) {
        let defaultChild = this.children[0]
        return defaultChild.getEntry(direction, origin, commonViewport)
      }

      return null
    },
    /**
     * Update translate values so that the specified Navigable is visible in the viewport instance
     * @param {Navigable} child
     */
    makeVisible (child) {
      if (this.canTranslate.x || this.canTranslate.y) {
        let cRect = child.rect
        let vRect = this.rect

        let deltaLeft = 0
        let deltaTop = 0
        if (this.canTranslate.x) {
          if (cRect.left < this.translatePadding.left) {
            deltaLeft = (cRect.left - this.translatePadding.left) * -1
          } else if (cRect.right > vRect.width - this.translatePadding.right) {
            deltaLeft = (cRect.right - (vRect.width - this.translatePadding.right)) * -1
          }
        }
        if (this.canTranslate.y) {
          if (cRect.top < this.translatePadding.top) {
            deltaTop = (cRect.top - this.translatePadding.top) * -1
          } else if (cRect.bottom > vRect.height - this.translatePadding.bottom) {
            deltaTop = (cRect.bottom - (vRect.height - this.translatePadding.bottom)) * -1
          }
        }

        this.translate.left += deltaLeft
        this.translate.top += deltaTop
      }
    },
    onChildFocus (child) {
      this.focusedChild = child
      this.focusedBy = this.focusedChild.focusedBy
      if (this.focusedBy !== 'mouse') this.makeVisible(this.focusedChild)
    },
    onChildBlur (child) {
      if (this.focusedChild === child) this.focusedChild = null
    },
    onChildMounted (child) {
      child.$on('focus', this.onChildFocus)
      child.$on('blur', this.onChildBlur)
      this.isEmpty = !this.children.length
      this.$emit('child-mounted')
    },
    onChildDestroyed (child) {
      child.$off('focus', this.onChildFocus)
      child.$off('blur', this.onChildBlur)
      this.isEmpty = !this.children.length
      this.$emit('child-destroyed')
    }
  }
}
