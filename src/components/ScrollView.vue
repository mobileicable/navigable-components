<template>
  <div class="scroll-view" :class="classNames">
    <div class="scroll-body" ref="scrollBody" :class="scrollBodyClassNames" :style="scrollBodyStyle">
      <slot></slot>
    </div>
  </div>
</template>

<style lang="scss" scoped>
.scroll-view {
  position: relative;
  overflow: hidden;
  .scroll-body {
    text-align: left;
    &.by-transition, &.by-transition3d {
      position: relative;
      backface-visibility: hidden;
      transform-style: preserve-3d;
    }
    &.by-position, &.by-margin {
      position: absolute;
      top: 0; left: 0;
      min-width: 100%;
      min-height: 100%;
    }
  }
}
</style>

<script>
import NavigationManager from '../managers/NavigationManager'
import NavigableViewport from '../mixins/NavigableViewport'

/**
 * @typedef {Object} ScrollStatus
 * @property {Boolean} top The control has space to scroll up
 * @property {Boolean} bottom The control has space to scroll down
 * @property {Boolean} left The control has space to scroll left
 * @property {Boolean} right The control has space to scroll right
 */

/**
 * A wrapper control which can contain arbitrary Navigable and non-Navigable components,
 * suitable for freeform content that requires possibly multiple scrolling directions.
 * @property {Number|String} [transitionDuration="0.2s"] - Duration of the translate transition, in CSS time format
 * @property {ScrollMode} [scrollMode="both"] Possible scroll directions
 * @property {Boolean} [smoothScroll=true] If false, navigation will not be triggered during transition
 * @augments NavigableViewport
 * @module ScrollView
 * @example
 * <!-- code as: -->
 * <scrollview :transitionDuration="0.2s" scrollMode="both">
 *   <!-- navigable content -->
 * </scrollview>
 * @example
 * <!-- render as: -->
 * <div class="scroll-view">
 *   <div class="scroll-body">
 *     <!-- navigable content -->
 *   </div>
 * </div>
 */
export default {
  mixins: [NavigableViewport],
  data () {
    return {
      isMounting: false,
      isMounted: false,
      translateMinMax: { topMin: 0, topMax: 0, leftMin: 0, leftMax: 0 }
    }
  },
  computed: {
    transitionDurationInSecond () {
      if (typeof this.transitionDuration === 'string') {
        let ratio = this.transitionDuration.toLowerCase().indexOf('ms') > -1 ? 1000 : 1
        return parseFloat(this.transitionDuration) / ratio
      }
      return this.transitionDuration
    },
    scrollBodyStyle () {
      let style = {}
      if (this.translateMethod === 'position') {
        style.left = this.translate.left + 'px'
        style.top = this.translate.top + 'px'
      } else if (this.translateMethod === 'margin') {
        style.marginLeft = this.translate.left + 'px'
        style.marginTop = this.translate.top + 'px'
      } else if (this.translateMethod === 'tranlate3d') {
        style.transform = 'translate3d(' + this.translate.left + 'px, ' + this.translate.top + 'px, 0)'
      } else {
        style.transform = 'translate(' + this.translate.left + 'px, ' + this.translate.top + 'px)'
      }
      if (this.isMounted && this.transitionDurationInSecond > 0) {
        if (this.translateMethod === 'position') {
          style.transition = 'top ' + this.transitionDurationInSecond + 's, left ' + this.transitionDurationInSecond + 's'
        } else if (this.translateMethod === 'margin') {
          style.transition = 'margin-top ' + this.transitionDurationInSecond + 's, margin-left ' + this.transitionDurationInSecond + 's'
        } else {
          style.transition = 'transform ' + this.transitionDurationInSecond + 's'
        }
      }
      return style
    },
    scrollWidth: {
      cache: false,
      get () { return this.scrollBody && this.scrollBody.scrollWidth }
    },
    scrollHeight: {
      cache: false,
      get () { return this.scrollBody && this.scrollBody.scrollHeight }
    },
    /**
     * Boolean values showing if the control has space to scroll along specific directions
     * @member {ScrollStatus}
     * @memberof module:ScrollView
     * @instance
     * @readonly
     */
    scrollStatus () {
      return {
        top: this.translate.top < this.translateMinMax.topMax,
        left: this.translate.left < this.translateMinMax.leftMax,
        bottom: this.translate.top > this.translateMinMax.topMin,
        right: this.translate.left > this.translateMinMax.leftMin
      }
    },
    /**
     * The internal scroll body DOM element which contains the rendered items
     * @member {HTMLDivElement}
     * @memberof module:ScrollView
     * @instance
     * @readonly
     */
    scrollBody () {
      return this.isMounting || this.isMounted ? this.$refs.scrollBody : null
    },
    classNames () {
      return {
        focused: this.isFocused,
        disabled: !this.isEnabled,
        empty: this.isEmpty,

        topmost: !this.scrollStatus.top,
        bottommost: !this.scrollStatus.bottom,
        leftmost: !this.scrollStatus.left,
        rightmost: !this.scrollStatus.right
      }
    },
    scrollBodyClassNames () {
      return {
        ['by-' + this.translateMethod]: true
      }
    }
  },
  props: {
    transitionDuration: {
      type: [Number, String],
      default: 0.2,
      validator (v) { return typeof v === 'string' ? (v.match(/^(\d*\.)?\d+(ms|s)$/gi) || v.match(/^(0*\.)?0+$/gi)) : true }
    },
    scrollMode: { // overwrite
      type: String,
      default: 'both',
      validator (v) {
        return ['horizontal', 'vertical', 'both', 'none'].indexOf(v) !== -1
      }
    },
    smoothScroll: {
      type: Boolean,
      default: true
    }
  },
  created () {
    this.onWheel = function (ev) {
      let dx = Math.abs(ev.deltaX) > Math.abs(ev.deltaY) ? ev.deltaX : 0
      let dy = Math.abs(ev.deltaY) >= Math.abs(ev.deltaX) ? ev.deltaY : 0
      if (ev.shiftKey) { let _t = dx; dx = dy; dy = _t }

      if ((dx < 0 && this.scrollStatus.left) ||
        (dx > 0 && this.scrollStatus.right) ||
        (dy < 0 && this.scrollStatus.top) ||
        (dy > 0 && this.scrollStatus.bottom)) {
        this.scrollBy(dx, dy)
        ev.stopPropagation()
        return false
      } // let the others handle if no way to scroll
    }.bind(this)

    this.$on('child-mounted', () => { this.updateScrollStatus() })
    this.$on('child-destroyed', () => { this.updateScrollStatus() })
    this.$on('child-focus', () => {
      if (this.transitionDurationInSecond > 0 && !this.smoothScroll) {
        NavigationManager.instance.suspend(this.transitionDurationInSecond * 1000)
      }
    })
  },
  mounted () {
    this.isMounting = true
    this.$el.addEventListener('wheel', this.onWheel)
    setTimeout(() => {
      this.updateScrollStatus()
      this.isMounted = true // delay style to remove initial transition
      this.isMounting = false
    })
  },
  destroyed () {
    this.$el.removeEventListener('wheel', this.onWheel)
  },
  methods: {
    /**
     * Scroll the control content by displacements
     * @param {Number} x Displacement along X-axis in pixels
     * @param {Number} y Displacement along Y-axis in pixels
     */
    scrollBy (x, y) {
      if (this.canTranslate.x) {
        let t = this.translate.left - x
        if (t > this.translateMinMax.leftMax) t = this.translateMinMax.leftMax
        if (t < this.translateMinMax.leftMin) t = this.translateMinMax.leftMin
        this.translate.left = t
      }
      if (this.canTranslate.y) {
        let t = this.translate.top - y
        if (t > this.translateMinMax.topMax) t = this.translateMinMax.topMax
        if (t < this.translateMinMax.topMin) t = this.translateMinMax.topMin
        this.translate.top = t
      }
    },
    /**
     * Recalculate scroll measurements and status, call this after manipulating the content in the control
     */
    updateScrollStatus () {
      // this function is needed for non-reactive rect and scrollHeight/Width
      let rect = this.dimensions
      this.translateMinMax.topMax = this.translatePadding.top
      this.translateMinMax.topMin = -(this.scrollHeight - rect.height + this.translatePadding.bottom)
      this.translateMinMax.leftMax = this.translatePadding.left
      this.translateMinMax.leftMin = -(this.scrollWidth - rect.width + this.translatePadding.right)
    }
  }
}
</script>
