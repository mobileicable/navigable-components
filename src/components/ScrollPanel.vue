<template>
  <div class="scroll-panel" :class="classNames">
    <div class="scroll-body" ref="scrollBody" :class="scrollBodyClassNames" :style="scrollBodyStyle">
      <slot></slot>
    </div>
  </div>
</template>

<style lang="scss" scoped>
.scroll-panel {
  position: relative;
  overflow: hidden;
  .scroll-body {
    &.by-transition, &.by-transition3d {
      position: relative;
      backface-visibility: hidden;
      transform-style: preserve-3d;
    }
    &.by-position, &.by-margin {
      position: absolute;
      top: 0; left: 0;
      min-width: 100%;
      min-height: 100%;
    }
  }
}
</style>

<script>
import Navigable from '../mixins/Navigable'

/**
 * A wrapper control which scrolls continuously instead of focusing on components,
 * suitable for long static text and image.
 * @property {Number|String} [transitionDuration="0.2s"] - Duration of the translate transition, in CSS time format
 * @property {ScrollMode} [scrollMode="both"] Possible scroll directions
 * @property {Number|Array} [scrollDelta=100] Distance in pixels to be translated for each scroll action, use array if different amounts are required for x- and y-axis
 * @property {Boolean} [selectToScroll=false] If true, the control has to be selected to enable scrolling, otherwise scrolling is enabled just by focusing
 * @augments Navigable
 * @module ScrollPanel
 * @example
 * <!-- code as: -->
 * <scrollpanel :transitionDuration="0.2s" scrollMode="both">
 *   <!-- navigable content -->
 * </scrollpanel>
 * @example
 * <!-- render as: -->
 * <div class="scroll-panel">
 *   <div class="scroll-body">
 *     <!-- navigable content -->
 *   </div>
 * </div>
 */
export default {
  mixins: [Navigable],
  data () {
    return {
      isMounting: false,
      isMounted: false,
      translateMinMax: { topMin: 0, topMax: 0, leftMin: 0, leftMax: 0 },
      canTranslate: {
        x: false,
        y: false
      },
      translate: {
        top: 0,
        left: 0
      },
      isSelected: false
    }
  },
  computed: {
    transitionDurationInSecond () {
      if (typeof this.transitionDuration === 'string') {
        let ratio = this.transitionDuration.toLowerCase().indexOf('ms') > -1 ? 1000 : 1
        return parseFloat(this.transitionDuration) / ratio
      }
      return this.transitionDuration
    },
    scrollBodyStyle () {
      let style = {}
      if (this.translateMethod === 'position') {
        style.left = this.translate.left + 'px'
        style.top = this.translate.top + 'px'
      } else if (this.translateMethod === 'margin') {
        style.marginLeft = this.translate.left + 'px'
        style.marginTop = this.translate.top + 'px'
      } else if (this.translateMethod === 'tranlate3d') {
        style.transform = 'translate3d(' + this.translate.left + 'px, ' + this.translate.top + 'px, 0)'
      } else {
        style.transform = 'translate(' + this.translate.left + 'px, ' + this.translate.top + 'px)'
      }
      if (this.isMounted && this.transitionDurationInSecond > 0) {
        if (this.translateMethod === 'position') {
          style.transition = 'top ' + this.transitionDurationInSecond + 's, left ' + this.transitionDurationInSecond + 's'
        } else if (this.translateMethod === 'margin') {
          style.transition = 'margin-top ' + this.transitionDurationInSecond + 's, margin-left ' + this.transitionDurationInSecond + 's'
        } else {
          style.transition = 'transform ' + this.transitionDurationInSecond + 's'
        }
      }
      return style
    },
    scrollWidth: {
      cache: false,
      get () { return this.scrollBody && this.scrollBody.scrollWidth }
    },
    scrollHeight: {
      cache: false,
      get () { return this.scrollBody && this.scrollBody.scrollHeight }
    },
    /**
     * Boolean values showing if the control has space to scroll along specific directions
     * @member {ScrollStatus}
     * @memberof module:ScrollPanel
     * @instance
     * @readonly
     */
    scrollStatus () {
      return {
        top: this.translate.top < this.translateMinMax.topMax,
        left: this.translate.left < this.translateMinMax.leftMax,
        bottom: this.translate.top > this.translateMinMax.topMin,
        right: this.translate.left > this.translateMinMax.leftMin
      }
    },
    /**
     * The internal scroll body DOM element which contains the rendered items
     * @member {HTMLDivElement}
     * @memberof module:ScrollPanel
     * @instance
     * @readonly
     */
    scrollBody () {
      return this.isMounting || this.isMounted ? this.$refs.scrollBody : null
    },
    classNames () {
      return {
        focused: this.isFocused,
        disabled: !this.isEnabled,
        selected: this.isSelected,
        empty: this.isEmpty,

        topmost: !this.scrollStatus.top,
        bottommost: !this.scrollStatus.bottom,
        leftmost: !this.scrollStatus.left,
        rightmost: !this.scrollStatus.right
      }
    },
    scrollBodyClassNames () {
      return {
        ['by-' + this.translateMethod]: true
      }
    }
  },
  watch: {
    scrollMode: {
      immediate: true,
      handler (v) {
        this.canTranslate.x = v === 'horizontal' || v === 'both'
        this.canTranslate.y = v === 'vertical' || v === 'both'
      }
    }
  },
  props: {
    transitionDuration: {
      type: [Number, String],
      default: 0.2,
      validator (v) { return typeof v === 'string' ? (v.match(/^(\d*\.)?\d+(ms|s)$/gi) || v.match(/^(0*\.)?0+$/gi)) : true }
    },
    scrollMode: { // overwrite
      type: String,
      default: 'both',
      validator (v) {
        return ['horizontal', 'vertical', 'both', 'none'].indexOf(v) !== -1
      }
    },
    scrollDelta: {
      type: [Number, Array],
      default: 100,
      validator (v) {
        return typeof v === 'number' ? v >= 0 : (v.length >= 2 && v.reduce((p, c) => p && typeof c === 'number' && c >= 0, true))
      }
    },
    selectToScroll: {
      type: Boolean,
      default: false
    }
  },
  created () {
    this.onWheel = function (ev) {
      if (this.selectToScroll && !this.isSelected) return true

      let dx = Math.abs(ev.deltaX) > Math.abs(ev.deltaY) ? ev.deltaX : 0
      let dy = Math.abs(ev.deltaY) >= Math.abs(ev.deltaX) ? ev.deltaY : 0
      if (ev.shiftKey) { let _t = dx; dx = dy; dy = _t }

      if ((dx < 0 && this.scrollStatus.left) ||
        (dx > 0 && this.scrollStatus.right) ||
        (dy < 0 && this.scrollStatus.top) ||
        (dy > 0 && this.scrollStatus.bottom)) {
        this.scrollBy(dx, dy)
        ev.stopPropagation()
        return false
      } // let the others handle if no way to scroll

      // interrupt the event anyway if selected
      if (this.isSelected) {
        ev.stopPropagation()
        return false
      }
    }.bind(this)

    this.$on('child-mounted', () => { this.updateScrollStatus() })
    this.$on('child-destroyed', () => { this.updateScrollStatus() })
    this.$on('click', () => {
      if (!this.selectToScroll) return
      this.isSelected = !this.isSelected
    })
  },
  mounted () {
    this.isMounting = true
    this.$el.addEventListener('wheel', this.onWheel)
    setTimeout(() => {
      this.updateScrollStatus()
      this.isMounted = true // delay style to remove initial transition
      this.isMounting = false
    })
  },
  destroyed () {
    this.$el.removeEventListener('wheel', this.onWheel)
  },
  methods: {
    /**
     * Scroll the control content by displacements
     * @param {Number} x Displacement along X-axis in pixels
     * @param {Number} y Displacement along Y-axis in pixels
     */
    scrollBy (x, y) {
      if (this.canTranslate.x) {
        let t = this.translate.left - x
        if (t > this.translateMinMax.leftMax) t = this.translateMinMax.leftMax
        if (t < this.translateMinMax.leftMin) t = this.translateMinMax.leftMin
        this.translate.left = t
      }
      if (this.canTranslate.y) {
        let t = this.translate.top - y
        if (t > this.translateMinMax.topMax) t = this.translateMinMax.topMax
        if (t < this.translateMinMax.topMin) t = this.translateMinMax.topMin
        this.translate.top = t
      }
    },
    /**
     * Recalculate scroll measurements and status, call this after manipulating the content in the control
     */
    updateScrollStatus () {
      // this function is needed for non-reactive rect and scrollHeight/Width
      let rect = this.dimensions
      this.translateMinMax.topMax = 0
      this.translateMinMax.topMin = -(this.scrollHeight - rect.height)
      this.translateMinMax.leftMax = 0
      this.translateMinMax.leftMin = -(this.scrollWidth - rect.width)
    },
    handleNavigationKeyDown (keyName) {
      if (keyName === 'enter') return false
      if (this.selectToScroll && !this.isSelected) return false

      let scrollStatus = this.scrollStatus
      let scrollDeltaX = typeof this.scrollDelta === 'number' ? this.scrollDelta : this.scrollDelta[0]
      let scrollDeltaY = typeof this.scrollDelta === 'number' ? this.scrollDelta : this.scrollDelta[1]

      let x = 0
      let y = 0
      if (this.canTranslate.y) {
        if (keyName === 'top' && scrollStatus[keyName]) y = -scrollDeltaY
        if (keyName === 'bottom' && scrollStatus[keyName]) y = scrollDeltaY
      }
      if (this.canTranslate.x) {
        if (keyName === 'left' && scrollStatus[keyName]) x = -scrollDeltaX
        if (keyName === 'right' && scrollStatus[keyName]) x = scrollDeltaX
      }

      if (x || y) {
        this.scrollBy(x, y)
        return true
      }
      // interrupt the event anyway if selected
      if (this.isSelected) return true
    }
  }
}
</script>
