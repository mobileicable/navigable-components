<template>
  <div class="grid-view" :class="classNames">
    <div class="scroll-body" ref="scrollBody" :class="scrollBodyClassNames" :style="scrollBodyStyle">
      <panel class="grid-view-item" :style="gridItemStyle" v-for="(item, index) in items" :key="index" :ref="'item-' + index" :index="index">
        <slot :item="item"></slot>
      </panel>
    </div>
  </div>
</template>

<style lang="scss" scoped>
.grid-view {
  position: relative;
  overflow: hidden;
  .scroll-body {
    text-align: left;
    white-space: nowrap;
    &.by-transition, &.by-transition3d {
      position: relative;
      backface-visibility: hidden;
      transform-style: preserve-3d;
    }
    &.by-position, &.by-margin {
      position: absolute;
      top: 0; left: 0;
      min-width: 100%;
      min-height: 100%;
    }
    .grid-view-item {
      width: 100px;
      height: 100px;
      position: absolute;
    }
  }
}
</style>

<script>
import NavigationManager from '../managers/NavigationManager'
import NavigableViewport from '../mixins/NavigableViewport'
import View from './View'

/**
 * A control which can consume an array as input and render it in a two-dimension matrix,
 * with the support of displaying items with colspan and rowspan.
 * @property {Number|String} [transitionDuration="0.2s"] - Duration of the translate transition, in CSS time format
 * @property {ScrollMode} [scrollMode="both"] Possible scroll directions
 * @property {Boolean} [smoothScroll=true] If false, navigation will not be triggered during transition
 * @property {Array} items The binding data items to be displayed
 * @property {Orientation} [orientation="vertical"] Orientation of items rendering
 * @property {Number} [gridSize=1] Number of rows/columns on the fixed size of the grid
 * @property {Number|Array|String} [itemSize="auto"] Width and height in pixels of an 1x1 item. (Edge length / [width, height] / "auto")
 * @property {String} [itemPropKey="grid"] By default item properties are assigned and updated under 'grid' property, update this to use another key
 * @augments NavigableViewport
 * @module GridView
 * @example
 * // To enable rowspan / colspan, add 'grid' (or the key set in itemPropKey) properties in each item
 * // by default all items are 1x1 if not set
 * var items = [{
 *   foo: 'bar',
 *   grid: {
 *     rowspan: 2,
 *     colspan: 2
 *   }
 * }]
 * @example
 * <!-- code as: -->
 * <gridview orientation="vertical" :items="[...]" :grid-size="3">
 *   <template slot-scope="scope" :item="scope.item">
 *     <!-- item template -->
 *   </template>
 * </gridview>
 * @example
 * <!-- render as: -->
 * <div class="grid-view">
 *   <div class="scroll-body">
 *     <div class="view grid-view-item">
 *       <!-- rendered template with item 0 -->
 *     </div>
 *     <div class="view grid-view-item">
 *       <!-- rendered template with item 1 -->
 *     </div>
 *     <div class="view grid-view-item">
 *       <!-- rendered template with item 2 -->
 *     </div>
 *     <!-- more... -->
 *   </div>
 * </div>
 * @summary Testing
 */
export default {
  mixins: [NavigableViewport],
  components: { panel: View },
  data () {
    return {
      isMounting: false,
      isMounted: false,
      translateMinMax: { topMin: 0, topMax: 0, leftMin: 0, leftMax: 0 }
    }
  },
  computed: {
    transitionDurationInSecond () {
      if (typeof this.transitionDuration === 'string') {
        let ratio = this.transitionDuration.toLowerCase().indexOf('ms') > -1 ? 1000 : 1
        return parseFloat(this.transitionDuration) / ratio
      }
      return this.transitionDuration
    },
    scrollBodyStyle () {
      let style = {}
      if (this.translateMethod === 'position') {
        style.left = this.translate.left + 'px'
        style.top = this.translate.top + 'px'
      } else if (this.translateMethod === 'margin') {
        style.marginLeft = this.translate.left + 'px'
        style.marginTop = this.translate.top + 'px'
      } else if (this.translateMethod === 'tranlate3d') {
        style.transform = 'translate3d(' + this.translate.left + 'px, ' + this.translate.top + 'px, 0)'
      } else {
        style.transform = 'translate(' + this.translate.left + 'px, ' + this.translate.top + 'px)'
      }
      if (this.isMounted && this.transitionDurationInSecond > 0) {
        if (this.translateMethod === 'position') {
          style.transition = 'top ' + this.transitionDurationInSecond + 's, left ' + this.transitionDurationInSecond + 's'
        } else if (this.translateMethod === 'margin') {
          style.transition = 'margin-top ' + this.transitionDurationInSecond + 's, margin-left ' + this.transitionDurationInSecond + 's'
        } else {
          style.transition = 'transform ' + this.transitionDurationInSecond + 's'
        }
      }
      return style
    },
    gridItemStyle () {
      let style = {}
      if (this.isMounted && this.transitionDurationInSecond > 0) {
        style.transition = 'top ' + this.transitionDurationInSecond + 's, '
        style.transition += 'left ' + this.transitionDurationInSecond + 's'
      }
      return style
    },
    scrollWidth: {
      cache: false,
      get () { return this.scrollBody && this.scrollBody.scrollWidth }
    },
    scrollHeight: {
      cache: false,
      get () { return this.scrollBody && this.scrollBody.scrollHeight }
    },
    /**
     * Boolean values showing if the control has space to scroll along specific directions
     * @member {ScrollStatus}
     * @memberof module:GridView
     * @instance
     * @readonly
     */
    scrollStatus () {
      return {
        top: this.translate.top < this.translateMinMax.topMax,
        left: this.translate.left < this.translateMinMax.leftMax,
        bottom: this.translate.top > this.translateMinMax.topMin,
        right: this.translate.left > this.translateMinMax.leftMin
      }
    },
    /**
     * The computed width and height in pixels of an 1x1 item.
     * For "auto" option, the size would be the rendered size
     * of an ".grid-view-item" element.
     * @member {Object}
     * @property {Number} width Width in pixels
     * @property {Number} height Height in pixels
     * @memberof module:GridView
     * @instance
     * @readonly
     */
    computedItemSize: {
      cache: false,
      get () {
        let width = 0
        let height = 0
        if (this.itemSize === 'auto') {
          let body = this.scrollBody
          let sizeDetector = document.createElement('div')
          sizeDetector.className = 'grid-view-item size-detect'
          body.appendChild(sizeDetector)
          // let cs = window.getComputedStyle(sizeDetector)
          width = sizeDetector.offsetWidth // + parseFloat(cs.getPropertyValue('margin-left')) + parseFloat(cs.getPropertyValue('margin-right'))
          height = sizeDetector.offsetHeight // + parseFloat(cs.getPropertyValue('margin-top')) + parseFloat(cs.getPropertyValue('margin-bottom'))
          body.removeChild(sizeDetector)
        } else if (typeof this.itemSize === 'number') {
          width = height = this.itemSize
        } else if (Array.isArray(this.itemSize)) {
          width = this.itemSize[0]
          height = this.itemSize[1]
        }
        return { width, height }
      }
    },
    classNames () {
      return {
        focused: this.isFocused,
        disabled: !this.isEnabled,
        empty: this.isEmpty,

        topmost: !this.scrollStatus.top,
        bottommost: !this.scrollStatus.bottom,
        leftmost: !this.scrollStatus.left,
        rightmost: !this.scrollStatus.right,
        [this.orientation]: true
      }
    },
    /**
     * The index of the current focus, -1 if nothing is currently focusing
     * @member {Number}
     * @memberof module:GridView
     * @instance
     * @readonly
     */
    focusedIndex () {
      return this.focusedChild ? this.$children.findIndex(c => c._uid === this.focusedChild._uid) : -1
    },
    /**
     * The data item of the current focus, null if nothing is currently focusing
     * @member {?*}
     * @memberof module:GridView
     * @instance
     * @readonly
     */
    focusedItem () {
      return this.focusedIndex >= 0 && this.items && this.focusedIndex < this.items.length ? this.items[this.focusedIndex] : null
    },
    /**
     * The internal scroll body DOM element which contains the rendered items
     * @member {HTMLDivElement}
     * @memberof module:GridView
     * @instance
     * @readonly
     */
    scrollBody () {
      return this.isMounting || this.isMounted ? this.$refs.scrollBody : null
    },
    scrollBodyClassNames () {
      return {
        ['by-' + this.translateMethod]: true
      }
    }
  },
  props: {
    items: {
      type: Array,
      default: () => []
    },
    transitionDuration: {
      type: [Number, String],
      default: 0.2,
      validator (v) { return typeof v === 'string' ? (v.match(/^(\d*\.)?\d+(ms|s)$/gi) || v.match(/^(0*\.)?0+$/gi)) : true }
    },
    scrollMode: { // overwrite
      type: String,
      default: 'both',
      validator (v) {
        return ['horizontal', 'vertical', 'both'].indexOf(v) !== -1
      }
    },
    orientation: {
      type: String,
      default: 'vertical',
      validator (v) {
        return ['horizontal', 'vertical'].indexOf(v) !== -1
      }
    },
    gridSize: {
      type: Number,
      default: 1
    },
    itemSize: {
      type: [String, Number, Array],
      default: 'auto',
      validator (v) {
        return typeof v === 'string' ? v === 'auto' : typeof v === 'number' ? v >= 0 : v.length && v.reduce((p, c) => (p && typeof c === 'number' && c >= 0), true)
      }
    },
    itemPropKey: {
      type: String,
      default: 'grid',
      validator (v) {
        return !!v.match(/^[a-zA-Z_$][0-9a-zA-Z_$]*$/g)
      }
    },
    smoothScroll: {
      type: Boolean,
      default: true
    }
  },
  watch: {
    items () {
      this.$nextTick(() => { // wait for the item ready in refs
        this.updateGrid()
      })
    },
    gridSize () {
      this.updateGrid()
    },
    orientation () {
      this.updateGrid()
    },
    itemSize () {
      this.updateGrid()
    }
  },
  created () {
    this.onWheel = function (ev) {
      let dx = Math.abs(ev.deltaX) > Math.abs(ev.deltaY) ? ev.deltaX : 0
      let dy = Math.abs(ev.deltaY) >= Math.abs(ev.deltaX) ? ev.deltaY : 0
      if (ev.shiftKey) { let _t = dx; dx = dy; dy = _t }

      if ((dx < 0 && this.scrollStatus.left) ||
        (dx > 0 && this.scrollStatus.right) ||
        (dy < 0 && this.scrollStatus.top) ||
        (dy > 0 && this.scrollStatus.bottom)) {
        this.scrollBy(dx, dy)
        ev.stopPropagation()
        return false
      } // let the others handle if no way to scroll
    }.bind(this)

    this.$on('child-mounted', () => { this.updateScrollStatus() })
    this.$on('child-destroyed', () => { this.updateScrollStatus() })
    this.$on('child-focus', () => {
      if (this.transitionDurationInSecond > 0 && !this.smoothScroll) {
        NavigationManager.instance.suspend(this.transitionDurationInSecond * 1000)
      }
    })
  },
  mounted () {
    this.isMounting = true
    this.$el.addEventListener('wheel', this.onWheel)
    this.updateGrid()
    setTimeout(() => {
      this.updateScrollStatus()
      this.isMounted = true // delay style to remove initial transition
      this.isMounting = false
    })
  },
  destroyed () {
    this.$el.removeEventListener('wheel', this.onWheel)
  },
  methods: {
    /**
     * Scroll the control content by displacements
     * @param {Number} x Displacement along X-axis in pixels
     * @param {Number} y Displacement along Y-axis in pixels
     */
    scrollBy (x, y) {
      if (this.canTranslate.x) {
        let t = this.translate.left - x
        if (t > this.translateMinMax.leftMax) t = this.translateMinMax.leftMax
        if (t < this.translateMinMax.leftMin) t = this.translateMinMax.leftMin
        this.translate.left = t
      }
      if (this.canTranslate.y) {
        let t = this.translate.top - y
        if (t > this.translateMinMax.topMax) t = this.translateMinMax.topMax
        if (t < this.translateMinMax.topMin) t = this.translateMinMax.topMin
        this.translate.top = t
      }
    },
    /**
     * Recalculate positions of grid items manually,
     * computed coordinations will be assigned to <code>item.grid.x</code> and <code>item.grid.y</code>
     * (or the key set in <code>itemPropKey</code>)
     */
    updateGrid () {
      let grid = [] // a 2D array storing which cell is used
      let current = { x: 0, y: 0 } // current position to try fitting the item

      let keys = this.orientation === 'vertical' ? {
        forwardDirection: 'x',
        sidewayDirection: 'y',
        forwardSpan: 'colspan',
        sidewaySpan: 'rowspan'
      } : {
        forwardDirection: 'y',
        sidewayDirection: 'x',
        forwardSpan: 'rowspan',
        sidewaySpan: 'colspan'
      }

      let max = {}
      max[keys.forwardDirection] = this.gridSize
      max[keys.sidewayDirection] = Number.POSITIVE_INFINITY

      let setGrid = (x, y, id) => {
        while (grid.length <= y && y < max.y) grid.push([])
        while (grid[y].length <= x && x < max.x) grid[y].push(false)
        grid[y][x] = id || true
      }
      let getGrid = (x, y) => {
        if (y >= max.y || x >= max.x) return true
        if (y >= grid.length || x >= grid[y].length) return false
        return grid[y][x]
      }
      let trySetBox = (x, y, w, h, id) => {
        for (let i = x; i < x + w; i++) {
          for (let j = y; j < y + h; j++) {
            if (getGrid(i, j)) return false
          }
        }
        for (let i = x; i < x + w; i++) {
          for (let j = y; j < y + h; j++) {
            setGrid(i, j, id)
          }
        }
        return true
      }
      let advance = (c = 1) => {
        if (current[keys.forwardDirection] + c >= max[keys.forwardDirection]) {
          current[keys.forwardDirection] = 0
          current[keys.sidewayDirection] += c
        } else {
          current[keys.forwardDirection] += c
        }
      }
      this.items.forEach((item, i) => {
        if (!item[this.itemPropKey]) item[this.itemPropKey] = {}
        let forwardSpan = item[this.itemPropKey][keys.forwardSpan] || 1
        let colspan = item[this.itemPropKey].colspan || 1
        let rowspan = item[this.itemPropKey].rowspan || 1
        if (forwardSpan <= max[keys.forwardDirection]) {
          let canSetBox = false
          while (!canSetBox) {
            canSetBox = trySetBox(current.x, current.y, colspan, rowspan, i + 1)
            if (canSetBox) {
              Object.assign(item[this.itemPropKey], { x: current.x, y: current.y })
            }
            advance(canSetBox ? forwardSpan : 1)
          }
          let itemSize = this.computedItemSize
          this.$refs['item-' + i][0].$el.style.display = 'block'
          this.$refs['item-' + i][0].$el.style.left = item[this.itemPropKey].x * itemSize.width + 'px'
          this.$refs['item-' + i][0].$el.style.top = item[this.itemPropKey].y * itemSize.height + 'px'
          this.$refs['item-' + i][0].$el.style.width = colspan * itemSize.width + 'px'
          this.$refs['item-' + i][0].$el.style.height = rowspan * itemSize.height + 'px'
        } else {
          // console.warn('[GridView] Item #' + i + ' is too big to fit in')
          this.$refs['item-' + i][0].$el.style.display = 'none'
        }
      })

      setTimeout(() => {
        this.updateScrollStatus()
      }, this.transitionDurationInSecond * 1000)
    },
    /**
     * Recalculate scroll measurements and status, call this after manipulating the content in the control
     */
    updateScrollStatus () {
      // this function is needed for non-reactive rect and scrollHeight/Width
      let rect = this.dimensions
      this.translateMinMax.topMax = this.translatePadding.top
      this.translateMinMax.topMin = -(this.scrollHeight - rect.height + this.translatePadding.bottom)
      this.translateMinMax.leftMax = this.translatePadding.left
      this.translateMinMax.leftMin = -(this.scrollWidth - rect.width + this.translatePadding.right)
    }
  }
}
</script>
