const JavaScriptObfuscator = require('webpack-obfuscator')

let plugins = []

// add obfuscation on production build
if (process.argv[2] && process.argv[2] === 'build') {
  console.info('\x1b[44m\x1b[30m INFO \x1b[0m Obfuscation WILL be included')
  plugins.push(new JavaScriptObfuscator({
    stringArray: false,
    seed: 1
  }))
}

module.exports = {
  configureWebpack: {
    plugins
  }
}
